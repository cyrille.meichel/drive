# API Drive

API to manage files in Google Drive

## Routes

- **GET /auth** Launch oauth2 authentication
- **GET /drive/files** List files
- **GET /drive/files/{id}** Download file
- **POST /drive/files** Create a file
- **PUT /drive/files/{id}** Update a file

## Body

For update and creation the following body must be passed:

```json
{
    "name": "foo.json",
    "mime-type": "application/json",
    "description": "hello world",
    "data": "{}"
}
```

## Headers

For each request on `/drive/files`, the following header must be passed:

```text
Authorisation: Bearer xxxxx
```

This token is provided at the end of the authentication process `/auth`

For update, additional headers:

```text
X-Name: foo.json
X-Description: hello world
```
