package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/rs/cors"

	"github.com/gorilla/mux"
)

func main() {

	conf, errConf := getConfig()
	if errConf != nil {
		log.Fatal(errConf)
	}

	router := mux.NewRouter()

	corsOpts := cors.New(cors.Options{
		AllowOriginFunc:  func(origin string) bool { return true },
		AllowCredentials: true,
		AllowedMethods: []string{
			http.MethodGet, //http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowedHeaders: []string{"*"},
	})

	makeGoogleRoutes(conf, router)
	makeDriveRoutes(conf, router)
	makeMockRoutes(conf, router)
	makeProfileRoutes(conf, router)

	log.Printf("Listening on :%d", conf.PortBinding)
	http.ListenAndServe(fmt.Sprintf(":%d", conf.PortBinding), corsOpts.Handler(router))
}
