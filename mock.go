package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func makeMockRoutes(conf Configuration, router *mux.Router) {
	router.HandleFunc("/", func(resp http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(resp, `
		<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mock</title>
  <style>
	body{font-family:sans-serif;font-size:14px}
	label{font-weight:bold}
	span{font-size:0.9em;}
  </style>
  <script>
  	window.onload = function () {
		const urlParams = new URLSearchParams(location.search);
		const token = urlParams.get('access-token');
		if (token) {
			localStorage.setItem('token', token);
			location = '%s';
		}
		const out = document.getElementById("token");
		if (out) {
			out.innerHTML = localStorage.getItem('token');
		}
	}
  </script>
</head>
<body>
  <div>
	<label>Token:</label>
	<span id="token"></span>
  </div>
</body>
</html>
		`, conf.Redirect)
	}).Methods("GET")
}
