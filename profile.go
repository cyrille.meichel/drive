package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
)

type profileGoogle struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	FamilyName    string `json:"family_name"`
	GivenName     string `json:"given_name"`
	Hd            string `json:"hd"`
	Locale        string `json:"locale"`
	Name          string `json:"name"`
	Picture       string `json:"picture"`
	VerifiedEmail bool   `json:"verified_email"`
}

func getGoogleProfile(ctx context.Context, auth *oauth2.Config, apiToken *oauth2.Token) (profileGoogle, error) {
	client := auth.Client(ctx, apiToken)

	resp, err := client.Get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json")
	if err != nil {
		return profileGoogle{}, err
	}

	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return profileGoogle{}, err
	}

	var p profileGoogle
	err = json.Unmarshal(contents, &p)
	if err != nil {
		return profileGoogle{}, err
	}

	return p, nil
}

func makeProfileRoutes(conf Configuration, router *mux.Router) {
	// list files
	router.HandleFunc("/profile", func(resp http.ResponseWriter, req *http.Request) {

		profile, errProfile := getGoogleProfile(req.Context(), configureGoogle(conf), newToken(req.Header.Get("Authorization")))
		if errProfile != nil {
			http.Error(resp, fmt.Sprintf("[profile] %s", errProfile.Error()), http.StatusInternalServerError)
			return
		}

		resp.Header().Set("Content-Type", "application/json")
		json.NewEncoder(resp).Encode(profile)

	}).Methods("GET")

}
