FROM debian:buster

RUN apt-get update && apt-get install -y ca-certificates

ADD drive /bin/drive-api
#ADD config.json /etc/drive-api/
VOLUME /etc/drive-api

EXPOSE 6543

CMD /bin/drive-api
