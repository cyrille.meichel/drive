module gitlab.com/cyrille.meichel/drive

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/rs/cors v1.7.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.22.0
)
