package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// Configuration is the app configuration
type Configuration struct {
	ID          string `json:"id"`
	Secret      string `json:"secret"`
	API         string `json:"api"`
	Redirect    string `json:"redirect"`
	Callback    string `json:"callback"`
	Login       string `json:"login"`
	PortBinding int    `json:"port-binding"`
	APIPrefix   string `json:"api-prefix"`
}

func loadConfig(filename string) (Configuration, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return Configuration{}, err
	}

	conf := Configuration{}
	errJSON := json.Unmarshal(data, &conf)
	return conf, errJSON
}

func getConfig() (Configuration, error) {
	if fileExists("config.json") {
		log.Println("config.json ")
		return loadConfig("config.json")
	}
	log.Println("config.json not found")
	if fileExists("/etc/drive-api/config.json") {
		log.Println("loading /etc/drive-api/config.json")
		return loadConfig("/etc/drive-api/config.json")
	}
	log.Println("/etc/drive-api/config.json not found")
	log.Println("loading /etc/drive-api.json")
	return loadConfig("/etc/drive-api.json")
}
