package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"google.golang.org/api/drive/v3"
)

type fileCreator struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	MimeType    string `json:"mime-type"`
	Data        string `json:"data"`
}

type fileOut struct {
	ID         string            `json:"id"`
	MimeType   string            `json:"mimeType"`
	Name       string            `json:"name"`
	Properties map[string]string `json:"properties"`
}

func getDriveService(ctx context.Context, conf Configuration, token *oauth2.Token) (*drive.Service, error) {

	authConfig := configureGoogle(conf)

	client := authConfig.Client(ctx, token)

	service, err := drive.New(client)

	if err != nil {
		fmt.Printf("Cannot create the Google Drive service: %v\n", err)
		return nil, err
	}

	return service, err
}

func makeDriveRoutes(conf Configuration, router *mux.Router) {
	// list files
	router.HandleFunc("/drive/files", func(resp http.ResponseWriter, req *http.Request) {

		log.Println("[GET] /drive/files")

		service, errService := getDriveService(req.Context(), conf, newToken(req.Header.Get("Authorization")))
		if errService != nil {
			http.Error(resp, fmt.Sprintf("[service] %s", errService.Error()), http.StatusInternalServerError)
			return
		}

		fileList, errDrive := service.Files.List().Do()
		if errDrive != nil {
			http.Error(resp, fmt.Sprintf("[drive] %s", errDrive.Error()), http.StatusInternalServerError)
			return
		}

		out := make([]fileOut, len(fileList.Files))

		for i, elt := range fileList.Files {
			out[i] = fileOut{
				ID:       elt.Id,
				Name:     elt.Name,
				MimeType: elt.MimeType,
			}
		}

		resp.Header().Set("Content-Type", "application/json")
		json.NewEncoder(resp).Encode(out)

	}).Methods("GET")

	// get file content
	router.HandleFunc("/drive/files/{id}", func(resp http.ResponseWriter, req *http.Request) {

		vars := mux.Vars(req)

		log.Printf("[GET] /drive/files/%s", vars["id"])

		service, errService := getDriveService(req.Context(), conf, newToken(req.Header.Get("Authorization")))
		if errService != nil {
			http.Error(resp, fmt.Sprintf("[service] %s", errService.Error()), http.StatusInternalServerError)
			return
		}

		fileGet := service.Files.Get(vars["id"])
		file, errDrive := fileGet.Do()
		if errDrive != nil {
			http.Error(resp, fmt.Sprintf("[drive] %s", errDrive.Error()), http.StatusInternalServerError)
			return
		}

		dataResp, errDownload := fileGet.Download()
		if errDownload != nil {
			http.Error(resp, fmt.Sprintf("[download] %s", errDownload.Error()), http.StatusInternalServerError)
			return
		}
		data, errBody := ioutil.ReadAll(dataResp.Body)
		defer dataResp.Body.Close()
		if errBody != nil {
			http.Error(resp, fmt.Sprintf("[body] %s", errBody.Error()), http.StatusInternalServerError)
			return
		}

		resp.Header().Set("Content-Type", file.MimeType)
		resp.Header().Set("X-Name", file.Name)
		resp.Header().Set("X-Description", file.Description)

		resp.Write(data)

	}).Methods("GET")

	// create file
	router.HandleFunc("/drive/files", func(resp http.ResponseWriter, req *http.Request) {

		log.Println("[POST] /drive/files")

		data, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			http.Error(resp, fmt.Sprintf("[body] %s", err.Error()), http.StatusInternalServerError)
			return
		}

		f := fileCreator{}
		errJSON := json.Unmarshal(data, &f)
		if errJSON != nil {
			http.Error(resp, fmt.Sprintf("[json] %s", errJSON.Error()), http.StatusInternalServerError)
			return
		}

		metadata := &drive.File{
			Name:        f.Name,
			Description: f.Description,
			MimeType:    f.MimeType,
			CreatedTime: time.Now().Format(time.RFC3339),
		}

		service, errService := getDriveService(req.Context(), conf, newToken(req.Header.Get("Authorization")))
		if errService != nil {
			http.Error(resp, fmt.Sprintf("[service] %s", errService.Error()), http.StatusInternalServerError)
			return
		}

		file, errDrive := service.Files.Create(metadata).Media(strings.NewReader(f.Data)).Do()
		if errDrive != nil {
			http.Error(resp, fmt.Sprintf("[drive] %s", errDrive.Error()), http.StatusInternalServerError)
			return
		}

		resp.Header().Set("Content-Type", "application/json")
		json.NewEncoder(resp).Encode(file)

	}).Methods("POST")

	// update file content
	router.HandleFunc("/drive/files/{id}", func(resp http.ResponseWriter, req *http.Request) {

		vars := mux.Vars(req)

		log.Printf("[PUT] /drive/files/%s", vars["id"])

		data, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			http.Error(resp, fmt.Sprintf("[body] %s", err.Error()), http.StatusInternalServerError)
			return
		}

		f := fileCreator{}
		errJSON := json.Unmarshal(data, &f)
		if errJSON != nil {
			http.Error(resp, fmt.Sprintf("[json] %s", errJSON.Error()), http.StatusInternalServerError)
			return
		}

		metadata := &drive.File{
			Name:        f.Name,
			Description: f.Description,
			MimeType:    f.MimeType,
		}

		service, errService := getDriveService(req.Context(), conf, newToken(req.Header.Get("Authorization")))
		if errService != nil {
			http.Error(resp, fmt.Sprintf("[service] %s", errService.Error()), http.StatusInternalServerError)
			return
		}

		file, errDrive := service.Files.Update(vars["id"], metadata).Media(strings.NewReader(f.Data)).Do()
		if errDrive != nil {
			http.Error(resp, fmt.Sprintf("[drive] %s", errDrive.Error()), http.StatusInternalServerError)
			return
		}

		resp.Header().Set("Content-Type", "application/json")
		json.NewEncoder(resp).Encode(file)

	}).Methods("PUT")

	// delete file
	router.HandleFunc("/drive/files/{id}", func(resp http.ResponseWriter, req *http.Request) {

		vars := mux.Vars(req)

		log.Printf("[DELETE] /drive/files/%s", vars["id"])

		service, errService := getDriveService(req.Context(), conf, newToken(req.Header.Get("Authorization")))
		if errService != nil {
			http.Error(resp, fmt.Sprintf("[service] %s", errService.Error()), http.StatusInternalServerError)
			return
		}

		errDrive := service.Files.Delete(vars["id"]).Do()
		if errDrive != nil {
			http.Error(resp, fmt.Sprintf("[drive] %s", errDrive.Error()), http.StatusInternalServerError)
			return
		}

		resp.Header().Set("Content-Type", "application/json")
		json.NewEncoder(resp).Encode(map[string]string{"status": "done", "id": vars["id"]})

	}).Methods("DELETE")
}
