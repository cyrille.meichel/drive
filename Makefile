SOURCE=$(shell find . -name '*.go' -not -name '*_test.go' | grep -v "^vendor/")
EXEC=drive-api

$(EXEC): $(SOURCE)
	go build -o $@

clean:
	rm $(EXEC)

package: debpacker.yml $(EXEC)
	debpacker make --force --config debpacker.yml && mv target/*.deb .

install: is_debian_arch
ifeq ($(shell which debpaker; echo $$?),1)
	@echo "*** debpacker is intalled ***"
else
	go install github.com/ovh/cds/tools/debpacker
endif

is_debian_arch:
	which dpkg

docker: $(EXEC)
	docker build -t drive-api . && docker tag drive-api drive-api:latest