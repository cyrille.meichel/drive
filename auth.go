package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func getRoute(conf Configuration, url string) string {
	/*re := regexp.MustCompile(`^https?://.*?/`)
	return fmt.Sprintf("/%s", re.ReplaceAllString(url, ""))*/
	fmt.Println(conf.APIPrefix)
	result := strings.Replace(url, conf.APIPrefix, "", -1)
	if result[0] != '/' {
		result = fmt.Sprintf("/%s", result)
	}
	return result
}

func newToken(authorization string) *oauth2.Token {
	splits := regexp.MustCompile(`\s+`).Split(authorization, 2)
	return &oauth2.Token{AccessToken: splits[1], TokenType: splits[0]}
}

func configureGoogle(conf Configuration) *oauth2.Config {
	return &oauth2.Config{
		RedirectURL:  conf.Callback,
		ClientID:     conf.ID,
		ClientSecret: conf.Secret,
		Scopes:       []string{"email", "profile", "https://www.googleapis.com/auth/drive.file"},
		Endpoint:     google.Endpoint,
	}
}

func makeGoogleRoutes(conf Configuration, router *mux.Router) {

	authConfig := configureGoogle(conf)

	log.Println("configuring route", getRoute(conf, conf.Login))
	router.HandleFunc(getRoute(conf, conf.Login), func(resp http.ResponseWriter, req *http.Request) {
		url := authConfig.AuthCodeURL("")
		redirects := conf.Redirect
		redirect := "redirect=" + redirects + "; Path=/; HttpOnly"
		resp.Header().Set("set-cookie", redirect)
		http.Redirect(resp, req, url, http.StatusFound)
	}).Methods("GET")

	log.Println("configuring route", getRoute(conf, conf.Callback))
	router.HandleFunc(getRoute(conf, conf.Callback), func(resp http.ResponseWriter, req *http.Request) {

		query := req.URL.Query()
		gCode := query.Get("code")

		log.Println("Code", gCode)
		apiToken, err := authConfig.Exchange(req.Context(), gCode)

		if apiToken != nil && err == nil {
			log.Println("Token", apiToken.AccessToken)
			params := url.Values{}
			params.Add("access-token", fmt.Sprintf("%s %s", apiToken.TokenType, apiToken.AccessToken))
			http.Redirect(resp, req, fmt.Sprintf("%s?%s", conf.Redirect, params.Encode()), http.StatusFound)
		} else {
			log.Println("No token", err)
		}

	}).Methods("GET")

}
